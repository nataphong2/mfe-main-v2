import ArticlePage, {
  getServerSideProps as serverSideProps,
} from "article/article";

export const getServerSideProps = serverSideProps;

export default ArticlePage;
