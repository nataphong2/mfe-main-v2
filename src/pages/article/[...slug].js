import ArticleSlug, {
  getServerSideProps as serverSideProps,
} from "article/article-slug";

export const getServerSideProps = serverSideProps;

export default ArticleSlug;
