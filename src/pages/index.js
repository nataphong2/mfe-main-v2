import React, { lazy } from "react";
import Head from "next/head";

// const CheckoutTitle = lazy(() => import("article/title"));

// typeof window !== "undefined" && console.log('Test: ', window.article);

const Home = () => {
  return (
    <>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/nextjs-ssr/home/public/favicon.ico" />
      </Head>
      <div
        style={{
          padding: "24px",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <div className="flex flex-col items-center">
          <h1 className="text-2xl font-bold">Welcom to home page!!!</h1>
          {/* <CheckoutTitle /> */}
        </div>
      </div>
    </>
  );
};

Home.getInitialProps = async (ctx) => {
  return {};
};

export default Home;
