import React from "react";

const Nav = () => (
  <nav
    style={{
      padding: "24px",
      boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.75);",
    }}
  >
    <ul
      style={{
        listStyle: "none",
        display: "flex",
        justifyContent: "flex-end",
        gap: "8px",
      }}
    >
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/article">Article</a>
      </li>
      {/* <li>
        <a href="/article">Promotion</a>
      </li> */}
    </ul>
  </nav>
);

export default Nav;
