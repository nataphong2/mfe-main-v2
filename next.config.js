const NextFederationPlugin = require("@module-federation/nextjs-mf");

const remotes = (isServer) => {
  const location = isServer ? "ssr" : "chunks";
  const articleRemoteUrl =
    process.env.REMOTE_ARTICLE_URL || "http://localhost:3001";

  return {
    article: `article@${articleRemoteUrl}/_next/static/${location}/remoteEntry.js`,
  };
};

module.exports = {
  images: {
    domains: ["img.salehere.co.th"],
  },
  webpack(config, options) {
    config.plugins.push(
      new NextFederationPlugin({
        name: "home",
        filename: "static/chunks/remoteEntry.js",
        exposes: {
          "./nav": "./src/components/nav.js",
        },
        remotes: remotes(options.isServer),
        shared: {},
        extraOptions: {},
      })
    );

    return config;
  },
};
